import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css'],
})
export class TimerComponent {
  @Input() timePassed: number = 0;
  @Input() onStart: any;
  @Input() onWait: any;
  @Input() onReset: any;
  hours: String = '';
  minutes: String = '';
  seconds: String = '';
  constructor() {}

  ngOnChanges() {
    let mathHours = Math.floor(this.timePassed / 3600);
    this.hours = (mathHours.toString().length === 1 ? '0' : '') + mathHours;

    let mathMinutes = Math.floor((this.timePassed % 3600) / 60);
    this.minutes =
      (mathMinutes.toString().length === 1 ? '0' : '') + mathMinutes;

    let mathSeconds = this.timePassed % 60;
    this.seconds =
      (mathSeconds.toString().length === 1 ? '0' : '') + mathSeconds;
  }
}
