import { Component } from '@angular/core';
import { interval, tap, of, delay } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  timer: number = 0;
  diff: number = 0;
  subscription: any;
  isRunning: boolean = false;
  prevent: boolean = true;
  constructor() {}

  onStartHandler = () => {
    if (!this.isRunning) {
      const timerSubscription = interval(1000)
        .pipe(map((val) => val + 1))
        .subscribe((val) => {
          this.timer = val + this.diff;
        });
      this.subscription = timerSubscription;
      this.isRunning = true;
    } else {
      this.subscription.unsubscribe();
      this.isRunning = false;
      this.timer = 0;
      this.diff = 0;
    }
  };
  onWaitHandler = () => {
    if (this.prevent) {
      this.prevent = false;
      of(this.prevent)
        .pipe(
          delay(500),
          tap(() => {
            this.prevent = true;
          })
        )
        .subscribe();
    } else {
      if (this.subscription) {
        this.subscription.unsubscribe();
      }
      this.diff = this.timer;
      this.isRunning = false;
    }
  };

  onResetHandler = () => {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    const timerSubscription = interval(1000).subscribe((val) => {
      this.timer = val;
      this.isRunning = true;
    });
    this.subscription = timerSubscription;
  };
}
